/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <map>
#include <sstream>
#include <string>

#include <wayfire/core.hpp>
#include <wayfire/util.hpp>
#include <wayfire/view.hpp>
#include <wayfire/object.hpp>
#include <wayfire/plugin.hpp>
#include <wayfire/matcher.hpp>
#include <wayfire/toplevel.hpp>
#include <wayfire/view-helpers.hpp>
#include <wayfire/toplevel-view.hpp>
#include <wayfire/window-manager.hpp>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/bindings-repository.hpp>
#include <wayfire/nonstd/wlroots-full.hpp>

std::string ltrim( const std::string& s ) {
    size_t start = s.find_first_not_of( ' ' );

    return (start == std::string::npos) ? "" : s.substr( start );
}


std::string rtrim( const std::string& s ) {
    size_t end = s.find_last_not_of( ' ' );

    return (end == std::string::npos) ? "" : s.substr( 0, end + 1 );
}


std::string trim( const std::string& s ) {
    return rtrim( ltrim( s ) );
}


std::vector<std::string> split( const std::string& s ) {
    std::stringstream        ss( s );
    std::vector<std::string> list;

    for ( std::string line; std::getline( ss, line, ',' );  ) {
        list.push_back( trim( line ) );
    }

    return list;
}


class wayfire_focus_request : public wf::plugin_interface_t {
    wayfire_view last_focus_view = nullptr;
    std::vector<wayfire_view> attention_list;

    wf::option_wrapper_t<wf::activatorbinding_t> focus_key{ "focus-request/focus_last_demand" };

    wf::activator_callback cb =
        [ = ] ( auto ) {
            if ( attention_list.size() == 0 ) {
                LOGE( "Nothing to focus" );
                return true;
            }

            /** Take the last element */
            wayfire_view view = attention_list.back();

            if ( view == nullptr ) {
                LOGE( "[FocusRequest] Nothing to activate." );
                return true;
            }

            LOGE( "[FocusRequest] Activating view ", view->get_app_id(), " (", view->get_id(), ") ", view->get_title() );

            /** Bring it to the front */
            wf::get_core().default_wm->focus_raise_view( view );

            LOGE( "[FocusRequest] Attention list size (before) ", attention_list.size() );

            /** Remove it from the list */
            attention_list.pop_back();

            LOGE( "[FocusRequest] Attention list size (after) ", attention_list.size() );

            return true;
        };

    wf::signal::connection_t<wf::view_focus_request_signal> onFocusRequested {
        [ = ] (wf::view_focus_request_signal *ev) {
            LOGD( "[FocusRequest] view_focus_request_signal intercepted: ", ev->view->get_app_id(), " (", ev->view->get_id(), ")" );
            /** Cast it into a toplevel object */
            auto toplevel = wf::toplevel_cast( ev->view );

            /** Worry about this only if it's a toplevel */
            if ( toplevel == nullptr ) {
                return;
            }

            LOGD( "[FocusRequest] Self-request: ", ev->self_request, "; carried out: ", ev->carried_out );

            /** Get the focus stealing timeout */
            wf::option_wrapper_t<int> focus_timeout{ "focus-request/focus_stealing_timeout" };

            LOGD( "[FocusRequest] FSP Timeout ", focus_timeout.value() );

            /** Grant focus to these views unconditionally */
            wf::view_matcher_t always_auto_focus{ "focus-request/auto_focus_views" };

            /** If this is one of the views which always get focus */
            if ( always_auto_focus.matches( ev->view ) ) {
                LOGD( "[FocusRequest] Granting focus to always-view ", toplevel->get_app_id(), " (", ev->view->get_id(), ")" );
                wf::get_core().default_wm->focus_raise_view( ev->view, true );

                /** Mark this as carried out */
                ev->carried_out = true;
            }

            /** Focus children of the current view unconditionally */
            wf::option_wrapper_t<bool> auto_focus_children{ "focus-request/auto_focus_children" };

            /** Parent of the current */
            auto parent_view = toplevel->parent;

            /** Check if this the child of the currently focused view. Grant focus if needed */
            if ( (parent_view != nullptr) && (auto_focus_children.value() == true) ) {
                auto current_focus = wf::get_core().get_cursor_focus_view();

                if ( (current_focus != nullptr) && (parent_view == current_focus) ) {
                    LOGD( "[FocusRequest] Granting focus to child ", toplevel->get_app_id(), " (", ev->view->get_id(), ")" );
                    wf::get_core().default_wm->focus_raise_view( toplevel );

                    /** Mark this as carried out */
                    ev->carried_out = true;

                    /** We're done here */
                    return;
                }
            }

            /** We're now interested only in self-requests */
            if ( ev->self_request and not ev->carried_out ) {
                /** We'll either deny or acquiesce this request. So mark as carried out. */
                ev->carried_out = true;

                LOGD( "[FocusRequest] Recieved focus request. ", toplevel->get_app_id(), " (", ev->view->get_id(), ")" );

                /** Grant it focus automatically, or just highlight it? */
                wf::option_wrapper_t<bool> auto_grant_focus{ "focus-request/auto_grant_focus" };

                /** If the request is to be granted without ado, let's do that */
                if ( auto_grant_focus.value() == true ) {
                    LOGD( "[FocusRequest] Granting focus ", toplevel->get_app_id(), " (", ev->view->get_id(), ")" );
                    wf::get_core().default_wm->focus_raise_view( toplevel );

                    /** We're done here */
                    return;
                }

                /**
                 * None of the above applied. So set demands attention flag and emit
                 * view_hints_changed_signal
                 */
                LOGD( "[FocusRequest] Setting demands-attention hint ", toplevel->get_app_id(), " (", ev->view->get_id(), ")" );
                wf::view_hints_changed_signal hints_signal;
                hints_signal.view = ev->view;
                hints_signal.demands_attention = true;
                ev->view->emit( &hints_signal );
                wf::get_core().emit( &hints_signal );
            }
        }
    };

    wf::signal::connection_t<wf::view_hints_changed_signal> onViewHintsChanged {
        [ = ] (wf::view_hints_changed_signal *ev) {
            /** Check if the view is in the list */
            auto it = std::find( attention_list.begin(), attention_list.end(), ev->view );

            LOGE( "[FocusRequest] View hints changed ", ev->view->get_app_id(), " (", ev->view->get_id(), ") ", ev->demands_attention );

            std::string appid = ev->view->get_app_id();
            if ( ( appid.size() == 0 ) || ( appid == "nil" ) || ( appid == " " ) ) {
                LOGE( "[FocusRequest] weird app (", ev->view->get_id(), ") ignoring." );
                return;
            }

            /** demands_attention was set on this view */
            if ( ev->demands_attention ) {
                /** Current view in the list: move it to the end */
                if ( it != attention_list.end() ) {
                    int idx = it - attention_list.begin();
                    std::swap( attention_list[ idx ], attention_list.back() );
                    LOGE( "[FocusRequest] attention list size ", attention_list.size() );
                }

                /** Current view not in the list: add it to the end */
                else {
                    attention_list.push_back( ev->view );
                    LOGE( "[FocusRequest] attention list size ", attention_list.size() );
                }

                ev->view->get_output()->connect( &onViewDisappeared );
            }

            /** demands_attention was removed from this view */
            else {
                /** Current view in the list: remove it */
                if ( it != attention_list.end() ) {
                    attention_list.erase( it );
                    LOGE( "[FocusRequest] attention list size ", attention_list.size() );
                }
            }
        }
    };

    wf::signal::connection_t<wf::view_disappeared_signal> onViewDisappeared {
        [ = ] (wf::view_disappeared_signal *ev) {
            if ( ev->view == nullptr ) {
                return;
            }

            /** Check if the view is in the list */
            auto it = std::find( attention_list.begin(), attention_list.end(), ev->view );

            if ( it != attention_list.end() ) {
                int idx = it - attention_list.begin();
                LOGE( "[FocusRequest] removing view ", ev->view->get_id(), " ", ev->view->get_app_id() );
                std::swap( attention_list[ idx ], attention_list.back() );
                LOGE( "[FocusRequest] attention list size ", attention_list.size() );
            }
        }
    };

    public:
        void init() override {
            wf::get_core().connect( &onFocusRequested );
            wf::get_core().connect( &onViewHintsChanged );

            /** Get the currently focused view */
            last_focus_view = wf::get_core().get_cursor_focus_view();

            /** Add the binding, this first time when the plugin is init */
            LOGE( "[FocusRequest] Adding activator" );

            /** Add this to the list of bindings */
            wf::get_core().bindings->add_activator( focus_key, &cb );
        }

        bool is_unloadable() override {
            return true;
        }

        void fini() override {
            wf::get_core().disconnect( &onFocusRequested );
        }
};

DECLARE_WAYFIRE_PLUGIN( wayfire_focus_request );
